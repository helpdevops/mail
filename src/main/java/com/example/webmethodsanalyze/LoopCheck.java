package com.example.webmethodsanalyze;

import java.util.logging.Logger;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class LoopCheck {
	
    private static final Logger logger = Logger.getLogger(WebMethodsAnalyzeApplication.class.getName());
    static Boolean Error=false;
    
    public static Boolean CheckLoops(Node node, String fileName) {
    	SearchforLoopandCheck(node, fileName);
    	return Error;
    }
	
	public static void SearchforLoopandCheck(Node node, String fileName) {
        if (node.getNodeType() == Node.ELEMENT_NODE) {
        	if(node.getNodeName().equals("LOOP")) {
        		if(checkInvokedServices(node)) {
        			System.out.println("Error");
        			logger.severe("The Flow Servie "+fileName+" use a loop with one of the invoked services <pub.list:appendToStringList> or <pub.list:appendToDocumentList> which should be avoided in prod because they lead to inefficient memory allocation and increased processing time. Each call creates a new array and copies the existing elements, causing high memory consumption and performance degradation. Instead, use more efficient methods like pre-allocating list sizes, using IDataUtil methods, or custom Java services. ");
					if(!Error) {
						Error=true;
					}        		}
        		}
        	
        	
            //System.out.println(node.getNodeName());
            NodeList children = node.getChildNodes();
            for (int i = 0; i < children.getLength(); i++) {
            	SearchforLoopandCheck(children.item(i),fileName);
                //if(children.item(i).getNodeName().equals("Values")) System.out.println("-------------------------rdr-------------");
            }
        }
    }
	
	public static Boolean checkInvokedServices(Node node) {
		NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
        	if(children.item(i).getNodeName().equals("INVOKE")) {
        		NamedNodeMap map=children.item(i).getAttributes();
        		Node node1=map.item(0);
    			String service=node1.getNodeValue();
    			if(service.equals("pub.list:appendToStringList")||service.equals("pub.list:appendToDocumentList")) {
    				 return true;
    			}
    			
    			}
            }
        
        return false;
	}

}
