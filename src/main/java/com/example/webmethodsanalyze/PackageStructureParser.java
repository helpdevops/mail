package com.example.webmethodsanalyze;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import java.util.logging.Logger;

@Component
public class PackageStructureParser {

    private static final Logger logger = Logger.getLogger(PackageStructureParser.class.getName());

    @Value("${connection.package:false}")
    private boolean connectionPackage;

    @Value("${adapter.package:false}")
    private boolean adapterPackage;
    private final FileService fileService = new FileService();
    private final FileType fileType = new FileType();
    private final AppConfig appConfig;

    @Autowired
    public PackageStructureParser(AppConfig appConfig) {
        this.appConfig = appConfig;
    }

    public boolean isConnectionPackage() {
        return connectionPackage;
    }

    public boolean isAdapterPackage() {
        return adapterPackage;
    }

    public PackageNode parsePackage(File zipFile) throws IOException {
        String packageName = zipFile.getName().replace(".zip", "");
        PackageNode root = new PackageNode(packageName, NodeType.PACKAGE);

        try (ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile))) {
            ZipEntry entry;
            while ((entry = zis.getNextEntry()) != null) {
                if (entry.getName().startsWith("ns/")) {
                    addEntryToTree(root, entry.getName(), zis);
                }
            }
        }

        return root;
    }

    private void handleFileTypeAssignment(PackageNode current, String part, ZipInputStream zis) throws IOException {
        if (part.endsWith(".ndf") || part.endsWith(".idf")) {
            NodeType determinedType = fileService.checkFileTypeFromStream(zis);
            current.setType(determinedType);

            if (determinedType == NodeType.CONNECTION_DATA) {
                connectionPackage = true;
            } else if (determinedType == NodeType.ADAPTER_SERVICE) {
                adapterPackage = true;
            }
        }
    }

    private void handleXmlProcessing(PackageNode current, String part, ZipInputStream zis) throws IOException, ParserConfigurationException, SAXException {
        if (part.endsWith(".xml") || part.endsWith(".ndf")) {
            byte[] xmlContent = StreamUtil.toByteArray(zis);
            PackageNode child = current.getChildren().stream()
                    .filter(node -> node.getName().equals(part))
                    .findFirst()
                    .orElse(null);

            if (child != null) {
            	//InputStream bais = StreamUtil.toByteArrayInputStream(new ByteArrayInputStream(xmlContent));
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                
               System.out.println(child.getPath());
                Document doc = builder.parse(child.getPath());

                doc.getDocumentElement().normalize();
                Node node = doc.getDocumentElement();
                System.out.println(node.getNodeName());
                child.setxmlContent(node);
            } else {
                logger.warning("Expected child node not found for part: " + part);
            }
        }
    }

    private PackageNode getChildNode(PackageNode current, String part, NodeType type) {
        PackageNode child = current.getChildren().stream()
                .filter(node -> node.getName().equals(part))
                .findFirst()
                .orElse(null);

        if (child == null) {
            child = new PackageNode(part, type);
            current.addChild(child);
        }
        return child;
    }




    private void addEntryToTree(PackageNode root, String entryPath, ZipInputStream zis) throws IOException {
        String[] parts = entryPath.split("/");
        PackageNode current = root;

        for (int i = 1; i < parts.length; i++) {
            String part = parts[i];
            NodeType type = NodeType.FOLDER;

            PackageNode child = getChildNode(current, part, type);
            if (part.contains(".")) {
                child.setType(NodeType.FILE);
            }

            if (i == parts.length - 1 ) {
                handleFileTypeAssignment(current, part, zis);
                try {
					handleXmlProcessing(current, part, zis);
				} catch (IOException | ParserConfigurationException | SAXException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }

            current = child;
        }
    }
}
