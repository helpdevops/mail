package com.example.webmethodsanalyze;

import org.springframework.stereotype.Component;

import java.util.List;


public class PackageStructurePrinter {

    public void printStructure(PackageNode node, String indent) {
        String validationMessage = NamingConventionVerifier.validateNodeName(node, node.getParent() != null ? node.getParent().getName() : node.getName());
        System.out.println(indent + node.getName() + " (" + node.getType() + ") - " + validationMessage);
        for (PackageNode child : node.getChildren()) {
            printStructure(child, indent + "  ");
        }
    }

}